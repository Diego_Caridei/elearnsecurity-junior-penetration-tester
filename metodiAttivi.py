__author__ = 'Guybrush'
import  http.client

print("** Questo programma ritorna la lista di metodi se OPTIONS è abilitato")

host = input("Inserisci l host/IP: ")
port = input("Inserisci la porta(default:80): ")

if(port==""):
    port=80

try:
    connection = http.client.HTTPConnection(host, port)
    connection.request('GET', host)
    response = connection.getresponse()
    print("Server response:",response.status)
    connection.close()
except ConnectionRefusedError:
    print("Connection failed")