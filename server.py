__author__ = 'Guybrush'
import socket

SERVER_ADDR = "192.168.1.5"
SERVER_PORT = 44444
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) #creiamo e configuriamo la socket
s.bind((SERVER_ADDR,SERVER_PORT))# la funzione bind collega il socket all'indirizzo e porta forniti
s.listen(1)#configuriamo la socket in ascolto
print("Il server è in esecuzione, attendere per una connessione")
#La funzione accept restituisce due valori
# connection è l'oggetto socket che useremo per inviare e ricevere i dati
#address contiene l'idirizzo del client che si è collegato
connection, address = s.accept()
print("Il client è connesso con questo indirizzo:", address)
#Ciclo infinito dove saranno stampati tutti dati inviati dal client
while 1:
    data = connection.recv(1024)
    if not data:
        break
    connection.sendall(b'--Messaggio ricevuto --\n')
    print(data.decode('utf-8'))
connection.close()